// array to hold count of dice totals
let count = [
  [1, 0],
  [2, 0],
  [3, 0],
  [4, 0],
  [5, 0],
  [6, 0],
  [7, 0],
  [8, 0],
  [9, 0],
  [10, 0],
  [11, 0],
  [12, 0]
];

// rolls two dice together 1000 times
for (let i = 0; i < 1000; i++) {
  let rollofDice1 = Math.floor(Math.random() * (7 - 1)) + 1;
  let rollofDice2 = Math.floor(Math.random() * (7 - 1)) + 1;
  let oneTotal = rollofDice1 + rollofDice2;

  //adds total of dice together to count array
  for (let j = 0; j < count.length; j++) {
    if (oneTotal === count[j][0]) {
      count[j][1] = count[j][1] + 1;
    }
  }
}

// writes count array to html
for (let i = 0; i < count.length; i++) {
  if (i > 0) {
    document.write(`<p>${i + 1} : ${count[i][1]}`);
  }
}
